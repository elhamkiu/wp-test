<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'admin1234' );

/** MySQL hostname */
define( 'DB_HOST', 'wp-test-db.cwuwau327upy.ap-southeast-2.rds.amazonaws.com' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'c%(I@ExH+9ZKb{?W*wU;^BwwSqxJYX#7toyO+Q0GGpL]C,t<)o )s(_q>tYQ{3-M' );
define( 'SECURE_AUTH_KEY',  '}[58(#8pjWNfCg|fQKj1Io-,g%)t;w%k&>8{0zjL}9z<9=*-Bk&JkLOWqPLBk+|L' );
define( 'LOGGED_IN_KEY',    'B|Y@T*6=M}[&^a#}>LY!C7yc)Gx.0_u5l`x<U[l|IyK+* `RT,Dt(PAbZ)vH_!qt' );
define( 'NONCE_KEY',        'o]J--9eX`Ggrb>C@}7V4{6r@H2jA&_6;%9Xey9)L J]n0Vh/u%r0K`4YLW) m%#1' );
define( 'AUTH_SALT',        ';RqQal_iB.FWG|P5z:$Ha]YfSvLo7QMLBBrxT?ys1UT5mUt}@KG}5![ijj9yH.;V' );
define( 'SECURE_AUTH_SALT', '9NR#o6w@q7s(;7j)xpLJ#`i<}z!ozed>L~(d}!A@  #W|Vx8Nb1pZpT,*H!9L[,p' );
define( 'LOGGED_IN_SALT',   '*Iw4}We][;vDAZaaMG)iX(zQNb`.J91Ye)A[{{L_dQ^fZtN~i7km{4eAB9+Q<-,Q' );
define( 'NONCE_SALT',       '&BrYwY}yL30(88`A|_UTba<7fb)OhW<!R3JPrlvecNBU[_<Nc-=2]pxu8-ocyD6Q' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
