#!/bin/bash
set -ex



echo "First arg: $1"
apk update && apk upgrade
apk add --no-cache python3
apk add zip -y
pip3 install --no-cache-dir --upgrade pip 
pip install awscli
pip install boto3
pip install -U python-dotenv

apk add php7-ldap
apk add php7-gd
apk add openssh


# aws s3 cp "s3://$S3_CONFIG_BUCKET/facade/$1" .env

# package up the application for deployment
zip -rv /tmp/artifact.zip ./* ./.ebextensions ./.elasticbeanstalk .npmrc

# run the deployment script
python3 provision/beanstalk_deploy.py 